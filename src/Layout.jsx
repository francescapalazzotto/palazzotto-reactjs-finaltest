import Navbar from "./components/Navbar/Navbar"
import Footer from "./components/Footer/Footer"

import { useOutlet } from "react-router-dom"

export default function Layout(){

    const outlet = useOutlet()

    return(
        <>
            <Navbar />
            {outlet}
            <Footer />
        </>
    )
}