import { combineReducers } from 'redux';
// import { counterSlice } from './example/example';
import { loginSlice } from './login/loginIndex';

export default combineReducers({
    // counter: counterSlice.reducer,
    token: loginSlice.reducer
});