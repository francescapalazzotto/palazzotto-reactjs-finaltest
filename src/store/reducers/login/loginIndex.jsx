import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    token: "",
    data: {
        nome: "",
        cognome: "",
        email: "",
        ruoli: []
    }
}

export const loginSlice = createSlice({
    name: 'token',
    initialState: {
        token: "",
        data: {
            nome: "",
            cognome: "",
            email: "",
            ruoli: []
        }
    },
    reducers: {
        setToken: (state, action) => {
            state.token = action.payload.token
            state.data.nome = action.payload.nome
            state.data.cognome = action.payload.cognome
            state.data.email = action.payload.email
            state.data.ruoli = action.payload.ruoli
        },
        removeToken: (state) => {
            state.token = ""
            state.data.nome = "" 
            state.data.cognome = ""
            state.data.email = ""
            state.data.ruoli = []
        }
    }
});

export const { setToken, removeToken } = loginSlice.actions;