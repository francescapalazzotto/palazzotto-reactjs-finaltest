import { useEffect, useState } from "react";
import { UserContext } from "./UserContext";
import { checkLoginCookie } from "../services/RESTServices";
import { cookieTypes } from "../services/URLs";

export default function UserContextProvider({children}){
    
    // Creating values for registration user
    const [userData, setUserData] = useState({
        nome: "",
        cognome: "",
        email: "",
        ruoli: []
    })

    useEffect(() => {
        // Extracting the jwt from the cookies
        const loginInfo = checkLoginCookie(cookieTypes.jwt)
        if( loginInfo != null ){
            setUserData({
                nome: loginInfo.nome,
                cognome: loginInfo.cognome,
                email: loginInfo.email,
                ruoli: loginInfo.ruoli
            })
        }

    }, [])


    return(
        <>
            <UserContext.Provider value={{userData, setUserData}}>
                {children}
            </UserContext.Provider>
        </>
    )
}