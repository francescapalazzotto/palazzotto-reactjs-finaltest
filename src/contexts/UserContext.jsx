import { createContext } from "react";

// Creazione iniziale del contesto utilizzando un oggetto vuoto
export const UserContext = createContext({})