import Fig1 from '../assets/image1-carousel.jpeg'
import Fig2 from '../assets/image2-carousel.jpeg'
import Fig3 from '../assets/image3-carousel.jpeg'
import Card from '../components/Card/Card'

import { useEffect } from 'react'

export default function Home(){

    useEffect(
        () => {
            document.title = "ACADEMY SI - Homepage"
        },
        []
    )

    return(
        <>
        <main>
            {/* SECTION ACADEMY INFO */}
            <div className="academy">
                <h2>JOIN US</h2>
                <p>
                Join the <b><i>Sistemi Informativi Academy</i></b> that offers 
                a wide range of IT courses to increase your skills and enter 
                the working world with the maximum of your potential.
                </p>
            </div>

            {/* CAROSELLO EVENTI ACADEMY */}
            <div className="academy-events">
                <div id="carouselExampleFade" class="carousel slide carousel-fade">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src={Fig1} class="d-block w-100" alt="Academy Event March" />
                            <div className="carousel-caption d-none d-md-block" style={{backgroundClip: "content-box", background: "white", opacity: "0.80", color: "black"}}>
                                <p>
                                <i><b>March</b></i> -  begins another month full of Research 
                                and Training! <br />
                                At #SistemiInformativi evolution and growth 
                                never stop and talents are always a priority.
                                </p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src={Fig2} class="d-block w-100" alt="Academy Roma fourth edition" />
                            <div className="carousel-caption d-none d-md-block" style={{backgroundClip: "content-box", background: "white", opacity: "0.80", color: "black"}}>
                                <p>
                                The <b><i>Academy2024</i></b> never stop! <br />
                                A new edition - the fourth of the year - 
                                is coming to Rome, 
                                once again in collaboration with  
                                 <a href="https://www.openjobmetis.it/it">Openjobmetis SpA</a> !
                                <br />
                                Very soon all the info!
                                </p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src={Fig3} class="d-block w-100" alt="Automation AI in Sistemi Informativi" />
                            <div className="carousel-caption d-none d-md-block" style={{backgroundClip: "content-box", background: "white", opacity: "0.80", color: "black"}}>
                                <p>
                                Sistemi Informativi uses models of 
                                <b><i> GenerativeAI</i></b> based on 
                                sophisticated algorithms, 
                                trained on large amounts of data and 
                                designed to learn patterns aimed at 
                                generating new content.
                                </p>
                            </div>
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div> 
            </div>

            {/* SECTION ABOUT US */}
            <div className="academy-about-us">
                <h2>ABOUT US</h2>
                <p>
                <b><i>Sistemi Informativi</i></b> is a company of the IBM group that has 
                always been committed to developing IT solutions for 
                the modernization and digitization of various industries 
                such as Public Administration, Banking and Insurance, 
                Telecommunications, Energy & Utilities, Travel&Transportation, 
                Consumer, Healthcare&Pharma.
                </p>
            </div>

            {/* CARD DOCENTI */}
            <div className="academy-teachers">
                <h2>TEACHERS</h2>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-3">
                            <Card 
                                name="Lorenzo Taverna"
                                field="Java Basics - HTML - CSS"
                                phone="+39 123 4567 890"
                                email="l.taverna@gmail.com" 
                            />
                        </div>
                        <div className="col-md-3">
                            <Card 
                                name="Mattia Caputo"
                                field="Java Advanced - Databases - SQL - AI"
                                phone="+39 098 7654 321"
                                email="m.caputo@gmail.com" 
                            />
                        </div>
                        <div className="col-md-3">
                            <Card 
                                name="Simone De Meis"
                                field="React JS - JavaScript"
                                phone="+39 123 0987 456"
                                email="simo.demeis@gmail.com" 
                            />
                        </div>
                        <div className="col-md-3">
                        <Card 
                                name="Piergiacomo De Ascaniis"
                                field="DevOps - Microservices Architectures"
                                phone="+39 789 0456 123"
                                email="pier.ascaniis@gmail.com" 
                            />
                        </div>
                    </div>
                </div>
            </div>
        </main>
        </>
    )
}