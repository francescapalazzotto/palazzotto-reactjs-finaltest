import { useNavigate } from "react-router-dom"
import { useEffect, useContext } from "react"
import { UserContext } from "../contexts/UserContext"

export default function Profile(){
    
    // Implementing the context
    const { userData, setUserData } = useContext(UserContext) 

    // Implementing navigation to the profile updating page
    const navigateTo = useNavigate()

    const handleClick = () => {
        navigateTo("/update-profile")
    }

    const handleClickLogout = () => {
        setUserData({
            nome: "",
            cognome: "",
            email: "",
            ruoli: []
        })
        console.log(userData)
        navigateTo("/")
    }

    // Obtaining the information of the user from the cookies
    const userName = userData.nome
    const userLastname = userData.cognome
    const userEmail = userData.email
    const userRoles = userData.ruoli

    useEffect(
        () => {
            document.title = "Welcome" + userName 
        },
        []
    )

    return(
        <>
            <hr style={{color: "#4169E1"}} />
            <div className="profile">

                {/* Header containing profile picture and data */}
                <div className="header-profile">
                    <div className="container">
                        <div className="row align-items-center">
                            {/* Image */}
                            <div className="col-md-4">
                            <img src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png" class="img-thumbnail" alt="Profile picture" width="200" height="200" />
                            </div>
                            {/* Data information */}
                            <div className="col-md-8">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">Name: {userName} </li>
                                    <li class="list-group-item">Lastname: {userLastname}  </li>
                                    <li class="list-group-item">Email: {userEmail} </li>
                                    <li class="list-group-item">Role: {userRoles} </li>
                                </ul>
                                <button className="btn btn-primary" 
                                    onClick={handleClick} 
                                    style={{position: "relative"}}>
                                    Update profile
                                </button> 
                                <button className="btn btn-danger" 
                                    onClick={handleClickLogout} 
                                    style={{position: "relative"}}>
                                    Logout
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <hr style={{color: "#4169E1"}} />
        </>
    )
}