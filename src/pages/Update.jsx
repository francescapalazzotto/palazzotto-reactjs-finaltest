import Cookies from "js-cookie";
import { useState, useEffect } from "react"
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";

export default function Update(){

    const regexNameLastname = /[a-zA-Zàèìòù]{1,50}$/;
    const navigateTo = useNavigate()

    const [updateData, setUpdateData] = useState({
        nome: "",
        cognome: "",
        email: "",
        id_g: ""
    })

    const handleChange = (e) => {
        const {name, value} = e.target
        setUpdateData({...updateData, [name]: value})
    }

    // PUT
    const fectUpdateData = async(updateData) => {

        const response = await fetch(
            "http://localhost:8080/api/utente/aggiornaUtente",
            {
                mode: "cors",
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(updateData)
            }
        )

        return response.status
    }

    const handleUpdateData = async() => {

        const status = await fectUpdateData(updateData)

        if(status == 200){
            console.log("Account updated successfully!")

            // Saving information of the cookies
            Cookies.remove("nome")
            Cookies.remove("cognome")
            Cookies.remove("email")

            Cookies.set("nome", updateData.nome, {expires: 1})
            Cookies.set("cognome", updateData.cognome, {expires: 1})
            Cookies.set("email", updateData.email, {expires: 1})
            Cookies.set("ruoli", updateData.id_g, {expires: 1})

            console.log(Cookies.get("nome"))
            console.log(Cookies.get("cognome"))
            console.log(Cookies.get("email"))
            console.log(Cookies.get("ruoli"))
            

        } else {
            console.log("Error during updating...")
        }
    }

    const handleSubmit = (e) => {

        e.preventDefault()
        if(
            regexNameLastname.test(updateData.nome) == false 
        ){
            alert("Incorrect data, please try again...")
        } else {
            handleUpdateData()
            navigateTo("/my-profile")
        }
    }

    useEffect(
        () => {
            document.title = "Update information" 
        },
        []
    )

    return(
        <>
        <hr style={{color: "#4169E1"}} />
            <div className="update-form">
                <h2>Update information</h2>
                <form onSubmit={handleSubmit}>
                    <div className="mb-3">
                        <label for="exampleInputEmail1" className="form-label">Name</label>
                        <input 
                            type="text" 
                            name="nome"
                            value={updateData.nome}
                            onChange={handleChange}
                            className="form-control"  
                            placeholder="Name" 
                        />
                    </div>

                    <div className="mb-3">
                        <label for="exampleInputEmail1" className="form-label">Lastname</label>
                        <input 
                            type="text" 
                            name="cognome"
                            value={updateData.cognome}
                            onChange={handleChange}
                            className="form-control"  
                            placeholder="Lastname"
                        />
                    </div>

                    <div className="mb-3">
                        <label for="exampleInputEmail1" className="form-label">Email address</label>
                        <input 
                            type="email" 
                            name="email"
                            value={updateData.email}
                            onChange={handleChange}
                            className="form-control" 
                            ariaDescribedby="emailHelp" 
                            placeholder="Email" 
                        />
                    </div>

                    <div className="mb-3">
                        <label for="exampleInputPassword1" className="form-label">Role</label>
                        <input 
                            type="text" 
                            name="id_g"
                            value={updateData.id_g}
                            onChange={handleChange}
                            className="form-control" 
                            placeholder="Role Id number - insert 0 if none"
                        />
                    </div>

                    <button type="submit" className="btn btn-primary">Save</button>
                </form>
            </div>
            <Link className="link-back-home" to="/">Home</Link>
            <hr style={{color: "#4169E1"}} />
        </>
    )
}