import { useState, useEffect, useContext } from "react"
import { Link, useNavigate } from "react-router-dom"
import { userRegistration } from "../services/RESTServices";

export default function Registration(){

    // Implementing regex for the submitting of data
    const regexNameLastname = /[a-zA-Zàèìòù]{1,50}$/;
    const regexEmail = /[A-z0-9.+_-]+@[A-z0-9._-]+.[A-z]{2,8}$/;
    const regexPass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@.#$!%*?&^])[A-Za-z\d@.#$!%*?&]{6,20}$/;

    // Implementing navigation to the profile page
    const navigateTo = useNavigate()

    // Implementing the data obtained from the registration form
    const [regData, setRegData] = useState({
        nome: "",
        cognome: "",
        email: "",
        password: ""
    })

    // Updating the changes to the form updating the variables
    const handleChange = (e) => {
        const {name, value} = e.target
        setRegData({...regData, [name]: value})
    }


    // Handling the call to the back-end
    const handleDataRegistration = async() => {

        const status = await userRegistration(regData)

        if(status == 200){
            console.log("Account created successfully!")
            
            // Navigate to the login page in order to gain data
            navigateTo("/user-login")
        } else {
            alert("Error during creation...")
            console.log("Error during creation...")
        }
    }    

    // Handling the submit event of the form 
    const handleSubmit = (e) => {
        // Preventing reloading page
        e.preventDefault()
        
        // Handling errors in submitting data:
        // if one of the field does not provide correct sematic, it returns
        // an alert - works also for empty fields
        if(
            regexNameLastname.test(regData.nome) == false ||
            regexNameLastname.test(regData.cognome) == false ||
            regexEmail.test(regData.email) == false ||
            regexPass.test(regData.password) == false
        ){
            alert("Incorrect data, please try again...")
        } else {
            // If data is correct, update the data inserted
            handleDataRegistration()
        }  
    }

    useEffect(
        () => {
            document.title = "Registration - ACADEMY SI"
        },
        []
    )

    return(
        <>
            <hr style={{color: "#4169E1"}} />
            <div className="registration-form">
                <h2>Create your account</h2>
                <form onSubmit={handleSubmit}>
                    <div className="mb-3">
                        <label for="exampleInputEmail1" className="form-label">Name</label>
                        <input 
                            type="text" 
                            name="nome"
                            value={regData.nome}
                            onChange={handleChange}
                            className="form-control"  
                            placeholder="Name" 
                        />
                    </div>

                    <div className="mb-3">
                        <label for="exampleInputEmail1" className="form-label">Lastname</label>
                        <input 
                            type="text" 
                            name="cognome"
                            value={regData.cognome}
                            onChange={handleChange}
                            className="form-control"  
                            placeholder="Lastname"
                        />
                    </div>

                    <div className="mb-3">
                        <label for="exampleInputEmail1" className="form-label">Email address</label>
                        <input 
                            type="email" 
                            name="email"
                            value={regData.email}
                            onChange={handleChange}
                            className="form-control" 
                            ariaDescribedby="emailHelp" 
                            placeholder="Email" 
                        />
                        <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div>
                    </div>

                    <div className="mb-3">
                        <label for="exampleInputPassword1" className="form-label">Password</label>
                        <input 
                            type="password" 
                            name="password"
                            value={regData.password}
                            onChange={handleChange}
                            className="form-control" 
                            placeholder="Password"
                        />
                    </div>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </form> <br />
                <Link className="link-back-home" to="/">Home</Link>
            </div>
            <hr style={{color: "#4169E1"}} />
        </>
    )
}