import { useState, useEffect, useContext } from "react"
import { useNavigate } from "react-router-dom"
import { jwtDecode } from "jwt-decode"
import { Link } from "react-router-dom"
import { UserContext } from "../contexts/UserContext"
import { userLogin } from "../services/RESTServices"



export default function Login(){

    const { userData, setUserData } = useContext(UserContext)

    // Implementing regex for the validation
    const regexEmail = /[A-z0-9.+_-]+@[A-z0-9._-]+.[A-z]{2,8}$/;
    const regexPass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@.#$!%*?&^])[A-Za-z\d@.#$!%*?&]{6,20}$/;

    // Implementing state of the form for the login 
    const [logData, setLogData] = useState({
        email: "",
        password: ""
    })

    // Navigation for the profile page
    const navigateTo = useNavigate()

    // Managing the inserted data
    const handleChange = (e) => {
        const {name, value} = e.target
        setLogData({...logData, [name]: value})
    }

    // Validation of data
    const handleSubmit = (e) => {
        e.preventDefault()

        if( regexEmail.test(logData.email) == false ||
            regexPass.test(logData.password) == false
        ){
            alert("Incorrect data, please try again...")
        } else {
            handleFetchLogin()
        }
    }

    const handleFetchLogin = async() => {

        const result = await userLogin(logData)

        if(result.status == 200){
            // Gets token authorization and save it into a cookie
            const token = await result.json()
            const jwtString = JSON.stringify(token)
            
            const decoded = jwtDecode(jwtString) // Ritorna le informazioni dell'utente
            console.log(decoded)

            setUserData(decoded)
            navigateTo("/my-profile")
        } else {
            alert("Not authorized, create an account..")
        }

    }

    useEffect(
        () => {
            document.title = "Login - ACADEMY SI"
        },
        []
    )


    return(
        <>
        <hr style={{color: "#4169E1"}} />
            <div className="login-form">
                <h2>Welcome!</h2>
                <form onSubmit={handleSubmit}>

                    <div className="mb-3">
                        <label for="exampleInputEmail1" className="form-label">Email address</label>
                        <input 
                            type="email" 
                            name="email"
                            value={logData.email}
                            onChange={handleChange}
                            className="form-control" 
                            ariaDescribedby="emailHelp" 
                            placeholder="Email" 
                        />
                    </div>

                    <div className="mb-3">
                        <label for="exampleInputPassword1" className="form-label">Password</label>
                        <input 
                            type="password" 
                            name="password"
                            value={logData.password}
                            onChange={handleChange}
                            className="form-control" 
                            placeholder="Password"
                        />
                    </div>

                    <button type="submit" 
                            className="btn btn-primary">
                        Login
                    </button>
                </form> <br />
                <Link className="link-back-home" to="/">Home</Link>
            </div>
            <hr style={{color: "#4169E1"}} />
        </>
    )
}