import Course from "../components/Course/Course"
import ModalForm from "../components/ModalForm/ModalForm.jsx"

import { Link, useNavigate } from "react-router-dom"
import { useContext, useEffect, useState } from "react"
import { createCourse, getCourses } from "../services/RESTServices.jsx"
import {UserContext} from '../contexts/UserContext.jsx'

export default function Courses(){
    
    const { userData } = useContext(UserContext)
    const [ courses, setCourses ] = useState([])

    const navigateTo = useNavigate()

    // Gaining all the courses available in the database
    const fetchCourses = async() => {
        const allCourses = await getCourses()
        setCourses(allCourses)
    }

    // Handling the case in which the user is not logged and can not see this page
    useEffect(() => {
        if( userData.nome == ""){
            navigateTo("/user-login")
        } else {
            document.title = "Courses - ACADEMY SI"
            fetchCourses()
        }
    }, [])

    const addCourse = async (course) => {
        const response = await createCourse(course)
        if( response == 200 ){
            fetchCourses()
        }
    }

    return(
        <>
        <Link to="/">Go back to the Home</Link>
        <hr style={{color: "#4169E1"}} />
        {/* Modal form to add a new course */}
        <ModalForm createCourse={addCourse} />

        {/* Lists of courses available */}
        {
            courses?.map((course) =>(
                <>
                <hr style={{color: "#4169E1"}} />
                <Course
                    key={Math.random()}
                    nameCourse={course.nome_corso}
                    idC={course.id_c}
                    shortDesc={course.descrizione_breve}
                    longDesc={course.descrizione_completa}
                    period={course.durata}
                    category={course.categoria.nome_categoria}
                    fetchData={fetchCourses}
                />
                <hr style={{color: "#4169E1"}} />
                </>
            ))
        }

        <hr style={{color: "#4169E1"}} />
        </>
    )
}