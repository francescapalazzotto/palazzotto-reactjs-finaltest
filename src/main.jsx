import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import '../node_modules/bootstrap/dist/js/bootstrap.bundle.js'
import '../node_modules/bootstrap/dist/css/bootstrap.css'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'

import UserContextProvider from './contexts/UserContextProvider.jsx'
import Layout from './Layout.jsx'
import Home from './pages/Home.jsx'
import Registration from './pages/Registration.jsx'
import Profile from './pages/Profile.jsx'
import Update from './pages/Update.jsx'
import Login from './pages/Login.jsx'
import Courses from './pages/Courses.jsx'

// Creating the router that contain all the paths to the pages
const router = createBrowserRouter(
  [
    {
      element: <UserContextProvider><Layout /></UserContextProvider>,
      children: [
        {
          path: "/",
          children: [
            {
              path: "",
              element: <Home />
            },
            {
              path: "registration",
              element: <Registration />,
            },
            {
              path: "my-profile",
              element: <Profile />
            },
            {
              path: "update-profile",
              element: <Update />
            },
            {
              path: "user-login",
              element: <Login />
            },
            {
              path: "all-courses",
              element: <Courses />
            }
          ]
        }   
      ]
    }
  ]
)

ReactDOM.createRoot(document.getElementById('root')).render(
  <RouterProvider router={router} />
)
