import { useContext } from "react"
import { Link } from "react-router-dom"
import { UserContext } from "../../contexts/UserContext"

export default function Navbar(){

    const { userData } = useContext(UserContext)

    return(
        <>
        <nav className="navbar navbar-expand-lg" style={{backgroundColor: "#A7C7E7"}}>
            <div className="container-fluid">
                <Link className="navbar-brand" to="/"> <img src="https://media.licdn.com/dms/image/C4D0BAQELTzC6BB1cDg/company-logo_200_200/0/1630553565989/sistemi_informativi_logo?e=1717632000&v=beta&t=GPtBF2cSSvUMOroAJwD5Er3Qncf6SOT9G_eh8jJ0SHs" alt="SI Logo" width="25" height="25" /> ACADEMY SI</Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div className="navbar-nav">
                        <Link className="nav-link active" aria-current="page" to="/">Home</Link> 
                        
                        <Link className="nav-link" to="/registration">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"/>
                            </svg>
                            Register
                        </Link>
                        
                        
                        <Link className="nav-link" to="/user-login">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-box-arrow-in-right" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M6 3.5a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 0-1 0v2A1.5 1.5 0 0 0 6.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-8A1.5 1.5 0 0 0 5 3.5v2a.5.5 0 0 0 1 0z"/>
                                <path fill-rule="evenodd" d="M11.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H1.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708z"/>
                            </svg>
                            Login
                        </Link>
                        {
                            userData.ruoli.includes("Admin") ? (
                                <Link 
                                    className="nav-link active" 
                                    aria-current="page" 
                                    to="/all-courses">Courses
                                </Link> 
                            ) : (
                                <></>
                            )
                        }

                    </div>
                </div>
            </div>
        </nav>
        </>
    )
}