import { delCourse } from "../../services/RESTServices"

export default function Course(props){

    const handleDeleteClick = async () => {

        // Call to the backend
        const response = await delCourse(props.idC)
        if( response == 200 ){
            props.fetchData()
        }
    }

    return(
        <>
        <dl>
            <dt>{props.nameCourse}</dt>
            <dd><i>Course Code:</i> {props.idC}</dd>
            <dd><i>Description:</i> {props.shortDesc}</dd>
            <dd><i>Details:</i> {props.longDesc}</dd>
            <dd><i>Duration:</i> {props.period} months</dd>
            <dd><i>Category:</i> {props.category}</dd>
        </dl>
        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            <button 
                class="btn btn-outline-danger btn-sm" 
                type="button"
                onClick={handleDeleteClick}>Delete course
            </button>
        </div>
        </>
    )
}