import { useEffect, useState, useContext } from "react"
import { UserContext } from "../../contexts/UserContext"
import { getTeachers, getUserId } from "../../services/RESTServices"
import { courseCategories } from "../../services/URLs"


export default function ModalForm(createCourse){

    const { userData } = useContext(UserContext)
    const [teachers, setTeachers] = useState([])

    const [newCourse, setNewCourse] = useState({
        nome_corso: "",
        descrizione_breve: "",
        descrizione_completa: "",
        durata: 0,
        categoria: {
            id_ca: 0,
            nome_categoria: ""
        },
        id_doc: 0
    }) 

    const handleChange = (e) => {
        const { name, value } = e.target
        setNewCourse({...newCourse, [name]: value})
        const idCategoria = courseCategories.newCourse.categoria.nome_categoria
        setNewCourse({id_ca: idCategoria})
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        createCourse(newCourse)
        setNewCourse({
            nome_corso: "",
            descrizione_breve: "",
            descrizione_completa: "",
            durata: 0,
            categoria: {
                id_ca: 0,
                nome_categoria: ""
            },
            id_doc: 0
        })
    }

    const fetchTeachers = async () =>{
        const teachers = await getTeachers()
        setTeachers(teachers)
    }

    useEffect(() => {
        // Call the function to get all the teachers available
        if( userData.nome == ""){
            navigateTo("/user-login")
        } else {
            fetchTeachers()
        }
    }, [])

    return(
        <>
            {/* <!-- Button trigger modal --> */}
            <button 
                type="button" 
                className="btn btn-primary" 
                dataBsToggle="modal" 
                dataBsTarget="#staticBackdrop">
            Add new course
            </button>

            {/* <!-- Modal --> */}
            <div 
                className="modal fade" 
                id="staticBackdrop" 
                dataBsBackdrop="static" 
                dataBsKeyboard="false" 
                tabindex="-1" 
                ariaLabelledby="staticBackdropLabel" 
                ariaHidden="true"
            >
                <div className="modal-dialog">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id="staticBackdropLabel">
                            New course
                        </h1>
                        <button 
                            type="button" 
                            className="btn-close" 
                            dataBsDismiss="modal" 
                            ariaLabel="Close"
                        ></button>
                    </div>
                    <div className="modal-body">
                        
                        <form id="form-new-course" onSubmit={handleSubmit}>
                            <div className="mb-3">
                                <input
                                    required
                                    name="nome_corso"
                                    type="text"
                                    className="form-new-course"
                                    id="nameCourse"
                                    placeholder="Course name"
                                    value={newCourse.nome_corso}
                                    onChange={handleChange}
                                />
                            </div>
                            <div className="mb-3">
                                <label>
                                    Period express in months:
                                    <select
                                        name="durata"
                                        form="form-new-course"
                                        required
                                        value={newCourse.durata}
                                        onChange={handleChange}
                                    >
                                        <option value="">Select the period</option>
                                        <option value="1">1 month</option>
                                        <option value="2">2 months</option>
                                        <option value="3">3 months</option>
                                        <option value="4">4 months</option>
                                        <option value="5">5 months</option>
                                        <option value="6">6 months</option>
                                    </select>
                                </label>
                                <div className="mb-3">
                                    <label>
                                    Category
                                    <select
                                        name="categoria"
                                        form="form-new-course"
                                        required
                                        value={newCourse.categoria.nome_categoria}
                                        onChange={handleChange}
                                    >
                                        <option value="">Select a category</option>
                                        <option value="FrontEnd">Front End</option>
                                        <option value="BackEnd">Back End</option>
                                        <option value="DeepLearning">Deep Learning</option>
                                    </select>
                                    </label>
                                </div>
                            </div>
                            <div className="mb-3">
                                <input
                                    required
                                    name="descrizione_breve"
                                    className="form-new-course"
                                    id="descrizione_breve"
                                    placeholder="Short description"
                                    value={newCourse.descrizione_breve}
                                    onChange={handleChange}
                                />
                            </div>
                            <div className="mb-3">
                                <textarea
                                    required
                                    name="descrizione_completa"
                                    className="form-new-course"
                                    id="descrizione_completa"
                                    placeholder="Complete description"
                                    value={newCourse.descrizione_completa}
                                    onChange={handleChange}
                                />
                            </div>
                            <div className="mb-3">
                                <label>
                                Teacher: 
                                <textarea
                                    name="id_doc"
                                    form="form-new-course"
                                    required
                                    placeholder="Insert one of the ID below"
                                    value={newCourse.id_doc}
                                    onChange={handleChange}
                                />
                                <ul>
                                    {
                                        teachers?.map( (t) => (
                                            <li key={Math.random}>
                                                {getUserId(t.email)}, 
                                                {t.nome} {t.cognome}
                                            </li>
                                        ))
                                    }
                                </ul>
                                </label>
                            </div>
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button 
                            type="button" 
                            className="btn btn-secondary" 
                            data-bs-dismiss="modal"
                        >Close</button>
                        <button 
                            type="submit" 
                            className="btn btn-primary"
                        >Add</button>
                    </div>
                    </div>
                </div>
            </div>
        </>
    )
}