// Constructing  an object containing all the urls used to call the back-end
export const URLs = {
    userRegistration: "http://localhost:8080/api/utente/registrazione",
    userUpdate: "http://localhost:8080/api/utente/aggiornaUtente",
    loginUtente: "http://localhost:8080/api/utente/login",
    getUtenteByEmail: "http://localhost:8080/api/utente", // Presents a query param
    
    // Admin only paths - courses
    createCorso: "http://localhost:8080/api/corso",
    getAllCourses: "http://localhost:8080/api/corso/getAll-courses",
    getUtenti: "http://localhost:8080/api/utente/getUtenti", // Presents a query param
    deleteCourse: "http://localhost:8080/api/corso/delete-course", // Presents a query param
    getIdUser: "http://localhost:8080/api/utente/get-id-from"
}

export const courseCategories = {
    FrontEnd: 1,
    BackEnd: 2,
    DeepLearning: 3
}

export const userRoles = {
    Admin: 1,
    Docente: 2,
    SuperAdmin: 3, // Not used
    Student: 4
}

export const jwtExpirations = {
    oneYear: new Date().getFullYear(),
    oneMonth: 31,
    oneWeek: 7,
    oneDay: 1
}

export const cookieTypes = {
    jwt: "JWT"
}