import Cookies from "js-cookie";
import { URLs, cookieTypes } from "./URLs";

// Registration
export async function userRegistration(formData){

    const response = await fetch(
        URLs.userRegistration,
        {
            mode: "cors",
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(formData)
        }
    )
    return response.status
}

// User Update
export async function userUpdate(formData){

    const response = await fetch(
        URLs.userUpdate,
        {
            mode: "cors",
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(formData)
        }
    )
    return response.status
}

// Login
export async function userLogin(formData){

    const response = await fetch(
        URLs.loginUtente,
        {
            mode: "cors",
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(formData)
        }
    )
    return response // Return a token 
}

// Get utente information
export async function userInformation(email){

    const response = await fetch(
        URLs.getUtenteByEmail + `?email=${email}`,
        {
            mode: "cors",
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        }
    )
    return response // Return user information 
}

//* ------
//* Implementing functions to handle token using cookies
// Saving token inside the cookies
export function setLoginCookie(jwtToken) {
    const jwtString = JSON.stringify(jwtToken.token)
    Cookies.set(cookieTypes.jwt, jwtString, {expires: jwtExpirations.oneMonth})
    return jwtDecode(jwtString)
}

// Checking if exists and then decode it 
export function checkLoginCookie(type){
    const loginCookie = Cookies.get(type)
    if(loginCookie != undefined){
      return jwtDecode(loginCookie)
    } else {
      return null
    }
}

// Extracting jwt from cooking if exists
export function getBearerToken(type){
    const loginCookie = Cookies.get(type)
    const shortToken = loginCookie.substring(1, loginCookie.length - 1)
    if(shortToken != undefined){
      return "Bearer " + shortToken
    } else {
      return null
    }
}
//* ------


// ADMIN
// Create a new course
export async function createCourse(data){

    const response = await fetch(
        URLs.createCorso,
        {
            mode: "cors",
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": getBearerToken(cookieTypes.jwt)
            },
            body: JSON.stringify(data)
        }
    )
    return response.status
}

// Getting all courses 
export async function getCourses(){

    const response = await fetch(
        URLs.getAllCourses,
        {
            mode: "cors",
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": getBearerToken(cookieTypes.jwt)
            }
        }
    )
    return response // Return the list of courses
}

// Getting all users 
export async function getUsers(){

    const response = await fetch(
        URLs.getUtenti,
        {
            mode: "cors",
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": getBearerToken(cookieTypes.jwt)
            }
        }
    )
    return response // Return the list of users
}

// Deleting a course
export async function delCourse(idCourse){

    const response = await fetch(
        URLs.deleteCourse + `?idCorso=${idCourse}`,
        {
            mode: "cors",
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                "Authorization": getBearerToken(cookieTypes.jwt)
            }
        }
    )
    return response.status
}

// Getting all the teachers
export async function getTeachers(){

    const response = await fetch(
        URLs.getUtenti + `?id_r=2`,
        {
            mode: "cors",
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": getBearerToken(cookieTypes.jwt)
            }
        }
    )
    return response // Return the list of teachers
}

// Getting user id using email
export async function getUserId(emailUser){

    const response = await fetch(
        URLs.getIdUser + `?email=${emailUser}`,
        {
            mode: "cors",
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": getBearerToken(cookieTypes.jwt)
            }
        }
    )
    return response // Return the id of the user
}